# determine our project name, either from CI_PROJECT_NAME which is normally set
# by GitLab CI or by looking at the name of our directory (that we are in)
ifneq ($(CI_PROJECT_NAME),)
PROJECT_NAME=$(CI_PROJECT_NAME)
else
PROJECT_NAME:=$(shell basename $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST)))))
endif

# compute docker tag to use for the docker images we produce
ifneq ($(CI_JOB_ID),)
DOCKER_TAG=$(CI_JOB_ID)
CNT_PREFIX=$(CI_JOB_ID)
else
DOCKER_TAG=$(shell whoami)-$(NSO_VERSION)
CNT_PREFIX=$(shell whoami)
endif

# Path for the NSO docker images (NSO_IMAGE_PATH) is derived based on
# information we get from Gitlab CI, if available. Similarly, the path we use
# for the images we produce is also based on information from Gitlab CI.
ifneq ($(CI_REGISTRY),)
NSO_IMAGE_PATH?=$(CI_REGISTRY)/$(CI_PROJECT_NAMESPACE)/nso-docker/
IMAGE_PATH?=$(CI_REGISTRY_IMAGE)/
PKG_PATH?=$(CI_REGISTRY)/$(CI_PROJECT_NAMESPACE)/
endif

.PHONY: test

all:
	$(MAKE) build
	$(MAKE) test
	$(MAKE) stop

build:
	docker build --target nso -t $(IMAGE_PATH)$(PROJECT_NAME):$(DOCKER_TAG) --build-arg NSO_IMAGE_PATH=$(NSO_IMAGE_PATH) --build-arg NSO_VERSION=$(NSO_VERSION) --build-arg PKG_PATH=$(PKG_PATH) .

test:
	$(MAKE) testenv-start
	$(MAKE) testenv-configure
	$(MAKE) testenv-test

stop:
	$(MAKE) testenv-stop
	$(MAKE) testenv-clean

push:
	docker push $(IMAGE_PATH)$(PROJECT_NAME):$(DOCKER_TAG)

tag-release:
	docker tag $(IMAGE_PATH)$(PROJECT_NAME):$(DOCKER_TAG) $(IMAGE_PATH)$(PROJECT_NAME):$(NSO_VERSION)

push-release:
	docker push $(IMAGE_PATH)$(PROJECT_NAME):$(NSO_VERSION)

testenv-start:
	-docker network create $(CNT_PREFIX)-$(PROJECT_NAME)
	docker run -td --network $(CNT_PREFIX)-$(PROJECT_NAME) --name $(CNT_PREFIX)-$(PROJECT_NAME)-nso --label testenv-$(CNT_PREFIX)-$(PROJECT_NAME) $${NSO_EXTRA_ARGS} $(IMAGE_PATH)$(PROJECT_NAME):$(DOCKER_TAG)
	docker run -td --network $(CNT_PREFIX)-$(PROJECT_NAME) --name $(CNT_PREFIX)-$(PROJECT_NAME)-netsim-ietf1 --network-alias ietf1 --label testenv-$(CNT_PREFIX)-$(PROJECT_NAME) $(PKG_PATH)docker-example-ned-ietf/docker-example-ned-ietf-netsim:$(NSO_VERSION)
	docker run -td --network $(CNT_PREFIX)-$(PROJECT_NAME) --name $(CNT_PREFIX)-$(PROJECT_NAME)-netsim-xr1 --network-alias xr1 --label testenv-$(CNT_PREFIX)-$(PROJECT_NAME) $(PKG_PATH)ned-iosxr-621/ned-iosxr-621-netsim:$(NSO_VERSION)
	docker exec -t $(CNT_PREFIX)-$(PROJECT_NAME)-nso bash -lc 'ncs --wait-started 600'
	docker exec -t $(CNT_PREFIX)-$(PROJECT_NAME)-nso bash -lc 'echo "show packages" | ncs_cli -u admin -g ncsadmin'
	docker exec -t $(CNT_PREFIX)-$(PROJECT_NAME)-nso bash -lc 'echo "show packages" | ncs_cli -u admin -g ncsadmin' | grep "oper-status up"

testenv-configure:
	docker cp test/add-device.xml $(CNT_PREFIX)-$(PROJECT_NAME)-nso:/add-device.xml
	docker exec -t $(CNT_PREFIX)-$(PROJECT_NAME)-nso bash -lc 'echo -e "configure\nload merge /add-device.xml\ncommit\nexit" | ncs_cli -u admin -g ncsadmin'
	docker exec -t $(CNT_PREFIX)-$(PROJECT_NAME)-nso bash -lc 'echo "show devices brief" | ncs_cli -u admin -g ncsadmin'
	docker exec -t $(CNT_PREFIX)-$(PROJECT_NAME)-nso bash -lc 'echo "request devices device * ssh fetch-host-keys" | ncs_cli -u admin -g ncsadmin'
	docker exec -t $(CNT_PREFIX)-$(PROJECT_NAME)-nso bash -lc 'echo "request devices device * sync-from" | ncs_cli -u admin -g ncsadmin'

testenv-test:
	docker cp test/configure-hostname-service.xml $(CNT_PREFIX)-$(PROJECT_NAME)-nso:/configure-hostname-service.xml
	docker exec -t $(CNT_PREFIX)-$(PROJECT_NAME)-nso bash -lc 'echo -e "configure\nload merge /configure-hostname-service.xml\ncommit\nexit" | ncs_cli -u admin -g ncsadmin'
	docker exec -t $(CNT_PREFIX)-$(PROJECT_NAME)-nso bash -lc 'echo "show configuration hostname" | ncs_cli -u admin -g ncsadmin'
	docker exec -t $(CNT_PREFIX)-$(PROJECT_NAME)-nso bash -lc 'echo -e "request hostname * re-deploy" | ncs_cli -u admin -g ncsadmin'
	docker exec -t $(CNT_PREFIX)-$(PROJECT_NAME)-nso bash -lc 'echo -e "show configuration devices device ietf1 config" | ncs_cli -u admin -g ncsadmin' | grep foobarhostname
	docker exec -t $(CNT_PREFIX)-$(PROJECT_NAME)-nso bash -lc 'echo -e "show configuration devices device xr1 config" | ncs_cli -u admin -g ncsadmin' | grep foobarhostname

testenv-stop:
	docker ps -q --filter label=testenv-$(CNT_PREFIX)-$(PROJECT_NAME) | xargs --no-run-if-empty docker stop

testenv-clean:
	docker ps -aq --filter label=testenv-$(CNT_PREFIX)-$(PROJECT_NAME) | xargs --no-run-if-empty docker rm -f
	-docker network rm $(CNT_PREFIX)-$(PROJECT_NAME)
	-docker volume rm $(CNT_PREFIX)-$(PROJECT_NAME)-packages


devenv-start:
	docker volume create $(CNT_PREFIX)-$(PROJECT_NAME)-packages
	$(MAKE) NSO_EXTRA_ARGS="$(NSO_EXTRA_ARGS) -v $(CNT_PREFIX)-$(PROJECT_NAME)-packages:/var/opt/ncs/packages" testenv-start

devenv-build:
	docker run -it --rm -v $(PWD):/src -v $(CNT_PREFIX)-$(PROJECT_NAME)-packages:/dst $(NSO_IMAGE_PATH)cisco-nso-dev:$(NSO_VERSION) bash -lc "rm -rf /dst/hostname; cp -a /src/hostname /dst/hostname; make -C /dst/hostname/src"
	docker exec -t $(CNT_PREFIX)-$(PROJECT_NAME)-nso bash -lc 'echo "request packages reload" | ncs_cli -u admin -g ncsadmin'
