ARG NSO_IMAGE_PATH
ARG NSO_VERSION
ARG PKG_PATH

# pull in remote images of NEDs and make them locally available through alias
FROM ${PKG_PATH}docker-example-ned-ietf/docker-example-ned-ietf:${NSO_VERSION} AS ietf-ned
FROM ${PKG_PATH}ned-iosxr-621/ned-iosxr-621:${NSO_VERSION} AS ned-iosxr-621

# compile local packages
FROM ${NSO_IMAGE_PATH}cisco-nso-dev:${NSO_VERSION} AS build

COPY hostname /var/opt/ncs/packages/hostname

RUN make -C /var/opt/ncs/packages/hostname/src


# build the final production NSO image
FROM ${NSO_IMAGE_PATH}cisco-nso-base:${NSO_VERSION} AS nso

# copy in already built NEDs from external repos
COPY --from=ietf-ned /var/opt/ncs/packages/ietf-ned /var/opt/ncs/packages/ietf-ned
COPY --from=ned-iosxr-621 /var/opt/ncs/packages/ned-iosxr-621 /var/opt/ncs/packages/ned-iosxr-621

# copy in local package
COPY --from=build /var/opt/ncs/packages/hostname /var/opt/ncs/packages/hostname
